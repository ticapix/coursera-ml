function [C, sigma] = dataset3Params(X, y, Xval, yval)
%EX6PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = EX6PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

C_range = [0.01, 0.03, 0.1, 0.3, 1.3, 10, 30];
sigma_range = [0.01, 0.03, 0.1, 0.3, 1.3, 10, 30];
[C_val, sigma_val] = meshgrid(C_range, sigma_range);
C_val = C_val(:);
sigma_val = sigma_val(:);
error = zeros(size(C_val));
for i = 1:size(C_val)
    fprintf("training for C: %f, sigma: %f\n", C_val(i), sigma_val(i));
    model = svmTrain(X, y, C_val(i), @(x1, x2) gaussianKernel(x1, x2, sigma_val(i))); 
    predictions = svmPredict(model, Xval);
    error(i) = mean(double(predictions ~= yval));
end
[_, i] = min(error);
C = C_val(i);
sigma = sigma_val(i);


% =========================================================================

end
